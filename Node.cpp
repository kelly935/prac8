#include "Node.h"

Node::Node()
{
    _data = 0;
    _next = nullptr;
}

//Get this nodes data
int Node::getData()
{
    return _data;
}
//Get the next node from this one
Node* Node::getNext()
{
    return _next;
}
//Set this nodes data
void Node::setData(int data)
{
    _data = data;
}
//Set the next node from this one
void Node::setNext(Node* next)
{
    _next = next;
}