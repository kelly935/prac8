#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include "Node.h"
#include "LinkedList.h"

// NOTE : you need to create a function to see if string is a function !
//         this function should return how many args the string function requires
// NOTE : secondly you need to create a flag to check if we have been passed a function 
//         so that we know to look for numbers and how many of them

bool isFuncStr(std::string str)
{
    std::vector<std::string> known_functions =  {
        "AF", //addFront
        "AE", //addEnd
        "AP", //addAtPosition
        "S",  //search
        "DF", //deleteFront
        "DE", //deleteEnd
        "DP", //deletePosition
        "GI"  //getItem
    };

    for(unsigned int i = 0; i < known_functions.size(); i++)
    {
        if(str == known_functions[i])
        {
            return true;
        }
    }

    return false;
}

int main()
{
    //Get line of user input
    std::string input, tmp, func;
    func = "";
    getline(std::cin, input);

    std::vector<int> vec_in, args_in, vec_out;

    std::stringstream stringstream(input);
    while(!stringstream.eof())
    {
        //Clear the tmp string and feed ss into tmp
        tmp = "";
        stringstream >> tmp;
        if(isFuncStr(tmp))
        {
            func = tmp; //Selects the function
        }
        else if(func == "")
        {
            vec_in.push_back(stoi(tmp));
        }
        else
        {
            args_in.push_back(stoi(tmp));
        }
    }

    LinkedList ll = LinkedList(vec_in.data(), vec_in.size());

    //Function Select
    if(func == "AF") ll.addFront(args_in[0]);
    else if(func == "AE") ll.addEnd(args_in[0]);
    else if(func == "AP") ll.addAtPosition(args_in[0], args_in[1]);
    else if(func == "S") ll.search(args_in[0]);
    else if(func == "DF") ll.deleteFront();
    else if(func == "DE") ll.deleteEnd();
    else if(func == "DP") ll.deletePosition(args_in[0]);
    else if(func == "GI") ll.getItem(args_in[0]);
    else std::cout << "Error: Undefined function!" << std::endl;

    ll.printItems();
}