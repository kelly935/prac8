#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#include "Node.h"

class LinkedList
{
    public:
        //Set head node to nullptr by default
        LinkedList();

        //Create linked list based on elements in the array, if size 0 head = nullptr
        LinkedList(int array[], int size);

        //On destruction delete each of the remaining nodes stored in the heap
        ~LinkedList();

        //Add node at the front of the list with value of 'newItem'
        void addFront(int newItem);

        //Add node at the end of the list with value of 'newItem'
        void addEnd(int newItem);

        //Add node at specified postion with value of 'newItem'
        void addAtPosition(int position, int newItem);
        
        //Search for item in list return and print (with space) the result
        int search(int item);

        //Get the item at position provided
        int getItem(int position);

        //Delete the node at the front of the list and clean up memory in heap
        void deleteFront();

        //Delete the node at the end of the list and clean up memory in heap
        void deleteEnd();

        //Delete the node at the position specified and clean up memory
        void deletePosition(int position);

        //Print all items in the list
        void printItems();

    private:

        //Node at the begining of the list - will be nullptr if empty list
        Node* _head;

};

#endif