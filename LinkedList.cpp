#include <iostream>
#include <limits>
#include "LinkedList.h"

LinkedList::LinkedList()
{
    _head = nullptr;
}

LinkedList::LinkedList(int array[], int size)
{
    _head = nullptr;

    for(int i = 0; i < size; i++)
    {
        addEnd(array[i]);
    }
}


void LinkedList::addFront(int newItem)
{
    //Set up the new node with the correct value
    Node* n = new Node();
    n->setData(newItem);

    //If head is not nullptr we need to attach old head to new node and make new node the head
    if(_head != nullptr)
    {
        n->setNext(_head);
        _head = n;   
    }
    //If head is nullptr just set head to the new node
    else
    {
        _head = n;
    }
}

void LinkedList::addEnd(int newItem)
{
    //Set up the new node with the correct value
    Node* n = new Node();
    n->setData(newItem);

    //If head is nullptr just set head to n
    if(_head == nullptr)
    {
        _head = n;
        return;
    }
    
    //Walk through the linked list until we find the end and attach the node
    Node* iter = _head;
    while(true)
    {
        //If the next node is nullptr then we are at the end
        if(iter->getNext() == nullptr)
        {
            iter->setNext(n);       
            break;
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }
}

void LinkedList::addAtPosition(int position, int newItem)
{
    //If position is less than 1 then we just add at the front
    if(position <= 1)
    {
        addFront(newItem);
        return;
    }

    //Set up the new node with the correct value
    Node* n = new Node();
    n->setData(newItem);

    //Walk through the linked list until we are at position or the end and attach the node
    Node* iter = _head;
    int counter = 1;

    
    while(true)
    {
        counter++;

        //If the next node is nullptr then we are at the end
        if(iter->getNext() == nullptr)
        {
            iter->setNext(n);       
            break;
        }
        
        //If we arrive at the position before end, attach node there
        else if(counter == position)
        {
            n->setNext(iter->getNext()); //Attach the next element to n
            iter->setNext(n); //Attach n to iter
            break;
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }
}

int LinkedList::search(int item)
{
    //Walk through the linked list until we find item or the end
    Node* iter = _head;
    int counter = 0;
    
    while(true)
    {
        counter++;

        //If we arrive at the position before end, return postion
        if(item == iter->getData())
        {
            std::cout << counter << " ";
            return counter;
        }

        //If the next node is nullptr then we are at the end
        if(iter->getNext() == nullptr)
        {
            //We have failed to find the item so we print and return 0
            std::cout << "0 ";
            return 0;
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }
}

void LinkedList::deleteFront()
{
    //If the head is null then we do nothing
    if(_head == nullptr)
    {
        return;
    }

    //If the next element is null then we just delete the head
    if(_head->getNext() == nullptr)
    {
        delete _head;
        _head = nullptr;
        return;
    }

    //Otherwise delete the head and set head to next node
    Node* next = _head->getNext();
    delete _head;
    _head = next;
    return;
}

void LinkedList::deleteEnd()
{
    //If the head is null then we do nothing
    if(_head == nullptr)
    {
        return;
    }

    //If the next element is null then we just delete the head
    if(_head->getNext() == nullptr)
    {
        delete _head;
        _head = nullptr;
        return;
    }

    //Otherwise walk through the linked list until we find the end
    Node* iter = _head;
    while(true)
    {
        //Look ahead two nodes, if we see a nullptr then
        if(iter->getNext()->getNext() == nullptr)
        {
            //Delete the last element
            delete iter->getNext();

            //Set iter to point to null
            iter->setNext(nullptr);
            break;
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }

}

void LinkedList::deletePosition(int position)
{
    if(position == 1)
    {
        deleteFront();
        return;
    }

    //If the head is null then we do nothing
    if(_head == nullptr)
    {
        return;
    }

    //If the next element is null then we just delete the head
    if(_head->getNext() == nullptr)
    {
        if(position != 1)
        {
            std::cout << "outside range" << std::endl;
            return;
        }

        delete _head;
        _head = nullptr;
        return;
    }

    //Otherwise walk through the linked list until we find the end
    Node* iter = _head;
    int counter = 1;

    while(true)
    {
        counter++;

        //Look ahead two nodes, if we see a nullptr then
        if(iter->getNext()->getNext() == nullptr)
        {
            if(counter != position)
            {
                std::cout << "outside range" << std::endl;
                return;
            }

            //Delete the last element
            delete iter->getNext();

            //Set iter to point to null
            iter->setNext(nullptr);
            break;
        }

        //If we arrive at the position before end, delete node at that position
        if(counter == position)
        {
            //Attach second next element to iter
            Node* tmp = iter->getNext()->getNext();

            //Delete the element at last position
            delete iter->getNext();

            iter->setNext(tmp);

            break;
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }

}

int LinkedList::getItem(int position)
{
    //Walk through the linked list until we are at position or the end
    Node* iter = _head;
    int counter = 0;

    while(true)
    {
        counter++;

        //If we arrive at the position before end, return postion
        if(counter == position)
        {
            std::cout << iter->getData() << " ";
            return iter->getData();
        }

        //If the next node is nullptr then we are at the end
        if(iter->getNext() == nullptr)
        {
            //We have failed to find the item so we print and return 0
            std::cout << std::numeric_limits<int>::max() << " ";
            return std::numeric_limits<int>::max();
        }
        
        //Otherwise get the next node
        iter = iter->getNext();
    }
}

void LinkedList::printItems()
{
    //If the head is null then we do nothing
    if(_head == nullptr)
    {
        return;
    }

    //Walk through the linked list printing out all values until we reach the end
    Node* iter = _head;
    while(true)
    {
        std::cout << iter->getData();
        iter = iter->getNext();
        
        //If the current node is nullptr we are at the end
        if(iter == nullptr)
        {
            std::cout << std::endl;
            break;
        }
        //Otherwise we can just print a space and get the next node
        else
        {
            std::cout << " ";
        }
    }
}

LinkedList::~LinkedList()
{
    if(_head == nullptr)
    {
        return;
    }

    //Walk through the linked list deleting each node until we reach the end
    Node* iter = _head;
    while(true)
    {   
        //If the current node is nullptr we are at the end
        if(iter == nullptr)
        {
            break;
        }
        //Otherwise we can just print a space and get the next node
        else
        {
            _head = iter;
            iter = iter->getNext();
            delete(_head);
        }
        
    }
}
