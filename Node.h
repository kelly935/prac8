#ifndef _NODE_H
#define _NODE_H

class Node
{
    public:
        Node();

        //Get this nodes data
        int getData();
        //Get the next node from this one
        Node* getNext();
        //Set this nodes data
        void setData(int data);
        //Set the next node from this one
        void setNext(Node *next);

    private:
        int _data;
        Node* _next;
};

#endif